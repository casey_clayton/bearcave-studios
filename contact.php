<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Bearcave Studio</title>
    
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<!-- bxslider -->
<link type="text/css" rel='stylesheet' href="js/bxslider/jquery.bxslider.css">
<!-- End bxslider -->
<!-- flexslider -->
<link type="text/css" rel='stylesheet' href="js/flexslider/flexslider.css">
<!-- End flexslider -->

<!-- bxslider -->
<link type="text/css" rel='stylesheet' href="js/radial-progress/style.css">
<!-- End bxslider -->

<!-- Animate css -->
<link type="text/css" rel='stylesheet' href="css/animate.css">
<!-- End Animate css -->

<!-- Bootstrap css -->
<link type="text/css" rel='stylesheet' href="css/bootstrap.min.css">
<link type="text/css" rel='stylesheet' href="js/bootstrap-progressbar/bootstrap-progressbar-3.2.0.min.css">
<!-- End Bootstrap css -->

<!-- Jquery UI css -->
<link type="text/css" rel='stylesheet' href="js/jqueryui/jquery-ui.css">
<link type="text/css" rel='stylesheet' href="js/jqueryui/jquery-ui.structure.css">
<!-- End Jquery UI css -->

<!-- Fancybox -->
<link type="text/css" rel='stylesheet' href="js/fancybox/jquery.fancybox.css">
<!-- End Fancybox -->

<link type="text/css" rel='stylesheet' href="fonts/fonts.css">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

<link type="text/css" data-themecolor="default" rel='stylesheet' href="css/main-default.css">

<link type="text/css" rel='stylesheet' href="js/rs-plugin/css/settings.css">
<link type="text/css" rel='stylesheet' href="js/rs-plugin/css/settings-custom.css">

</head>
<body>
<div class="mask-l" style="background-color: #fff; width: 100%; height: 100%; position: fixed; top: 0; left:0; z-index: 9999999;"></div> <!--removed by integration-->
<header>
  <div class="container b-header__box b-relative">
    <a href="/" class="b-left b-logo "><img class="color-theme" data-retina src="img/logo-header-default.png" alt="Logo" /></a>
    <div class="top_right">
    <div class="top_contact"><i class="fa fa-phone"></i> 123 456 7890</div><div class="top_contact"><i class="fa fa-envelope"></i> info@bearcavestudio.com</div>
    	<div class="top_social"><a href="#"><i class="fa  fa-facebook"></i></a><a href="#"><i class="fa  fa-twitter"></i></a><a href="#"><i class="fa  fa-youtube-square"></i></a></div>
    </div>
    <div class="b-header-r b-right b-header-r--icon">
        
    
      
      <div class="b-top-nav-show-slide f-top-nav-show-slide b-right j-top-nav-show-slide"><i class="fa fa-align-justify"></i></div>
      <nav class="b-top-nav f-top-nav b-right j-top-nav">
          <ul class="b-top-nav__1level_wrap">
    <li class="b-top-nav__1level f-top-nav__1level f-primary-b"><a href="index.html"><i class="fa fa-home b-menu-1level-ico"></i>Home </a>
        
    </li>
    <li class="b-top-nav__1level f-top-nav__1level  f-primary-b">
        <a href="about.html"><i class="fa fa-folder-open b-menu-1level-ico"></i>About Us</a>
       
    </li>
    <li class="b-top-nav__1level f-top-nav__1level f-primary-b">
        <a href="internet-marketing.html"><i class="fa fa-picture-o b-menu-1level-ico"></i> Marketing </a>
        
    </li>
    <li class="b-top-nav__1level f-top-nav__1level f-primary-b">
        <a href="web-development.php"><i class="fa fa-code b-menu-1level-ico"></i> Development </a>
        
    </li>
    <li class="b-top-nav__1level f-top-nav__1level f-primary-b">
        <a href="web-design.php"><i class="fa fa-cloud-download b-menu-1level-ico"></i>Web Design</a>
        
    </li>
    <li class="b-top-nav__1level f-top-nav__1level f-primary-b">
        <a href="portfolio.php"><i class="fa fa-inbox   b-menu-1level-ico"></i>Portfolio </a>
     </li>
    <li class="b-top-nav__1level is-active-top-nav__1level f-top-nav__1level f-primary-b">
        <a href="contact-us.html"><i class="fa fa-code b-menu-1level-ico"></i>Connect </a>
        
    </li>
    
    
</ul>

      </nav>
    </div>
  </div>
</header>
<div class="j-menu-container"></div>



<div class="b-inner-page-header f-inner-page-header b-bg-header-inner-page">
  <div class="b-inner-page-header__content">
    <div class="container">
      <h1 class="f-primary-l c-default">Contact us</h1>
    </div>
  </div>
</div>

<div class="l-main-container">
    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li><i class="fa fa-angle-right"></i><span> Contact us </span></li>
            </ul>
        </div>
    </div>
    <section class="b-google-map map-theme b-bord-box" data-location-set="contact-us">
        <div class="b-google-map__map-view b-google-map__map-height">
    <!-- Google map load -->
</div>
<img data-retina src="img/google-map-marker-default.png" data-label="" class="marker-template hidden" />
<div class="b-google-map__info-window-template hidden"
     data-selected-marker="0"
     data-width="250">
    <div class="b-google-map__info-window f-center b-google-map__info-office f-google-map__info-office">
    <h4 class="f-primary-b">Bearcave Studio</h4>
    <small>Heading Office</small>
</div>
</div>
    </section>
    <div class="b-desc-section-container">
        <section class="container b-welcome-box">
            <div class="row">
                <div class="col-md-offset-2 col-md-8">
                    <h1 class="is-global-title f-center">We’d love to hear from you!</h1>
                    <p class="f-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a scelerisque turpis, ut porta turpis. Integer imperdiet aliquet velit, vel tincidunt lectus dictum sed. Curabitur dignissim ut massa vel tincidunt. Nullam imperdiet pharetra ipsum in lobortis. Etiam convallis, felis quis dapibus dictum, libero mauris luctus nunc, non fringilla purus ligula non justo. Nullam </p>
                </div>
            </div>
        </section>
        <section class="container">
            <div class="row">
                <div class="col-sm-6 b-contact-form-box">
                    <h3 class="f-primary-b b-title-description f-title-description">
                        drop a line
                        <div class="b-title-description__comment f-title-description__comment f-primary-l">Quisque at tortor a libero posuere laoreet vitae sed arcu nunc at augue tincidunt </div>
                    </h3>
                    <div class="row">
                        <form action="send_form_email.php" method="post">
                            <div class="col-md-6">
                                <div class="b-form-row">
                                    <label class="b-form-vertical__label" for="name">Your name</label>
                                    <div class="b-form-vertical__input">
                                        <input type="text" id="name" class="form-control" />
                                    </div>
                                </div>
                                <div class="b-form-row">
                                    <label class="b-form-vertical__label" for="email">Your email</label>
                                    <div class="b-form-vertical__input">
                                        <input type="text" id="email" class="form-control" />
                                    </div>
                                </div>
                                <div class="b-form-row">
                                    <label class="b-form-vertical__label" for="website">Your website</label>
                                    <div class="b-form-vertical__input">
                                        <input type="text" id="website" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="b-form-row b-form--contact-size">
                                    <label class="b-form-vertical__label">Your message</label>
                                    <textarea class="form-control" rows="5"></textarea>
                                </div>
                                <div class="b-form-row">
                                    <a href="#" class="b-btn f-btn b-btn-md b-btn-default f-primary-b b-btn__w100">send message</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-sm-6 b-contact-form-box">
                    <h3 class="f-primary-b b-title-description f-title-description">
                        contact info
                        <div class="b-title-description__comment f-title-description__comment f-primary-l">Quisque at tortor a libero posuere laoreet vitae sed arcu nunc at augue tincidunt </div>
                    </h3>
                    <div class="row b-google-map__info-window-address">
                        <ul class="list-unstyled">
    <li class="col-xs-12">
        <div class="b-google-map__info-window-address-icon f-center pull-left">
            <i class="fa fa-home"></i>
        </div>
        <div>
            <div class="b-google-map__info-window-address-title f-google-map__info-window-address-title">
               BearCave Studios
            </div>
            <div class="desc">1234 Street Name, City Name, United States.</div>
        </div>
    </li>
    <li class="col-xs-12">
        <div class="b-google-map__info-window-address-icon f-center pull-left">
            <i class="fa fa-globe"></i>
        </div>
        <div>
            <div class="b-google-map__info-window-address-title f-google-map__info-window-address-title">
                Portfolio
            </div>
            <div class="desc">http://www.bearcave-studios.com/portfolio.html</div>
        </div>
    </li>
    <li class="col-xs-12">
        <div class="b-google-map__info-window-address-icon f-center pull-left">
            <i class="fa fa-skype"></i>
        </div>
        <div>
            <div class="b-google-map__info-window-address-title f-google-map__info-window-address-title">
                Skype
            </div>
            <div class="desc">ask.company</div>
        </div>
    </li>
    <li class="col-xs-12">
        <div class="b-google-map__info-window-address-icon f-center pull-left">
            <i class="fa fa-envelope"></i>
        </div>
        <div>
            <div class="b-google-map__info-window-address-title f-google-map__info-window-address-title">
                email
            </div>
            <div class="desc">contact@bearcave-studios.com</div>
        </div>
    </li>
</ul>

                    </div>
                </div>
            </div>
        </section>
    </div>
    
</div>
