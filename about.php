<?php
$page_title = 'About';
$section = 'about';
include 'partials/nav.php';
?>
<div class="b-inner-page-header f-inner-page-header b-bg-header-inner-page">
    <div class="b-inner-page-header__content">
        <div class="container">
            <h1 class="f-primary-l c-default">About us</h1>
        </div>
    </div>
</div>
<div class="l-main-container">
    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li><i class="fa fa-angle-right"></i><a href="#">About us</a></li>
            </ul>
        </div>
    </div>
    <section class="b-diagonal-line-bg-light b-infoblock--small ">
        <div class="container">
            <div class="row b-col-default-indent">
                <div class="col-md-6 col-xs-12">
                <img src="img/portfolio1.jpg" width="450" height="292"> </div>
                <div class="col-md-6 col-xs-12">
                    <h3 class="f-primary-b">WELCOME TO OUR COMPANY</h3>
                    <p class="f-primary-l">We are currently still working on finishing up the redesign so please bare with us!</p>
                    <ul class="c-primary c--inherit b-list-markers f-list-markers b-list-markers--without-leftindent f-list-markers--medium c-primary--all f-color-primary b-list-markers-2col f-list-markers-2col">
                        <li><a href="services.php"><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i> Web and Graphic Design</a></li>
                        <li><a href="services.php"><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i> Website Development</a></li>
                        <li><a href="services.php"><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i> Mobile App Developmemt</a></li>
                        <li><a href="services.php"><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i> CMS Integration, Customization, and Setup</a></li>
                        <li><a href="services.php"><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i> Ecommerce customization/setup</a></li>
                    </ul>
                    <div class="b-btn-container">
                        <a href="contact_us.html" class="b-btn f-btn b-btn-md f-btn-md b-btn-default f-primary-b button-gray-light">CONTACT US</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="b-infoblock">
        <div class="container">
            <h3 class="f-primary-b">Meet our team</h3>
            <p>The BearCave Studios team has years of experience in the technology industry.</p>
            <div class="b-shortcode-example">
                <div class="b-employee-container row">
                    <div class="col-md-6 col-sm-4 col-xs-12">
                        <div class="b-employee-item b-employee-item--color f-employee-item">
                            <div class=" view view-sixth">
                                <a href="#"><img class="j-data-element" data-animate="fadeInDown" data-retina src="img/users/user1.jpg" alt=""/></a>
                                <div class="b-item-hover-action f-center mask">
                                    <div class="b-item-hover-action__inner">
                                        <div class="b-item-hover-action__inner-btn_group">
                                            <a href="about_us_meet_our_team_detail.html" class="b-btn f-btn b-btn-light f-btn-light info"><i class="fa fa-link"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h4 class="f-primary-b">Casey Clayton</h4>
                            <div class="b-employee-item__position f-employee-item__position">ceo, founder, and lead developer</div>
                            <p>No Bio for Casey at this time.</p>
                            <div class="b-employee-item__social">
                                <a href="#" class="b-employee-item__social_btn"><i class="fa fa-twitter"></i></a>
                                <a href="#" class="b-employee-item__social_btn"><i class="fa fa-google-plus"></i></a>
                                <a href="#" class="b-employee-item__social_btn"><i class="fa fa-thumbs-up"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-4 col-xs-12 hidden-xs hidden-sm">
                        <div class="b-employee-item b-employee-item--color f-employee-item">
                            <div class=" view view-sixth">
                                <a href="#"><img class="j-data-element" data-animate="fadeInDown" data-retina src="img/users/user4.jpg" alt=""/></a>
                                <div class="b-item-hover-action f-center mask">
                                    <div class="b-item-hover-action__inner">
                                        <div class="b-item-hover-action__inner-btn_group">
                                            <a href="about_us_meet_our_team_detail.html" class="b-btn f-btn b-btn-light f-btn-light info"><i class="fa fa-link"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h4 class="f-primary-b">Devon Batson</h4>
                            <div class="b-employee-item__position f-employee-item__position">Lead Designer</div>
                            <p>Devon is experienced in the art of design having many years of expereince working on a variaty of projects.</p>
                            <div class="b-employee-item__social">
                                <a href="#" class="b-employee-item__social_btn"><i class="fa fa-twitter"></i></a>
                                <a href="#" class="b-employee-item__social_btn"><i class="fa fa-google-plus"></i></a>
                                <a href="#" class="b-employee-item__social_btn"><i class="fa fa-thumbs-up"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php include 'partials/footer.php'; ?>