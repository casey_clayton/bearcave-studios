<?php
$page_title = 'Home';
$section = 'index';
include 'partials/nav.php';
include 'partials/slider.php';
?>
<section class="b-desc-section-container b-diagonal-line-bg-light">
    <div class="container">
        <h2 class="f-center f-primary-b">why choose us ?</h2>
        <p class="b-desc-section f-desc-section f-center f-primary-l">We have years of expereince creating top of the line websites and applications.  Using the latest technologies we will make your project come to life. </p>
        <div class="b-hr-stars f-hr-stars">
            <div class="b-hr-stars__group">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
            </div>
        </div>
        <div class="b-infoblock-with-icon-group row">

            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="b-infoblock-with-icon">
                    <a href="#" class="b-infoblock-with-icon__icon f-infoblock-with-icon__icon fade-in-animate">
                    <i class="fa fa-tint"></i>
                    </a>
                    <div class="b-infoblock-with-icon__info f-infoblock-with-icon__info">
                        <a href="servies.php" class="f-infoblock-with-icon__info_title b-infoblock-with-icon__info_title f-primary-sb">Web Design</a>
                        <div class="f-infoblock-with-icon__info_text b-infoblock-with-icon__info_text">
                            HTML5, CSS3(Sass or Less), and Javascript(CoffeeScript) for a fully interactive and wonderful customer experience.
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix visible-sm-block"></div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="b-infoblock-with-icon">
                    <a href="#" class="b-infoblock-with-icon__icon f-infoblock-with-icon__icon fade-in-animate">
                    <i class="fa fa-th-large"></i>
                    </a>
                    <div class="b-infoblock-with-icon__info f-infoblock-with-icon__info">
                        <a href="services.php" class="f-infoblock-with-icon__info_title b-infoblock-with-icon__info_title f-primary-sb">Web Applications</a>
                        <div class="f-infoblock-with-icon__info_text b-infoblock-with-icon__info_text">
                            Need a CMS or Ecommerece?  No problem we have expereince with many OpenSource platforms such as Magento and Wordpress.  Our team specializes in custom web applications, we will build your application from the ground up to fit your needs.
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix visible-md-block visible-lg-block"></div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="b-infoblock-with-icon">
                    <a href="#" class="b-infoblock-with-icon__icon f-infoblock-with-icon__icon fade-in-animate">
                    <i class="fa  fa-mobile"></i>
                    </a>
                    <div class="b-infoblock-with-icon__info f-infoblock-with-icon__info">
                        <a href="services.php" class="f-infoblock-with-icon__info_title b-infoblock-with-icon__info_title f-primary-sb">Mobile Applications</a>
                        <div class="f-infoblock-with-icon__info_text b-infoblock-with-icon__info_text">
                            Need a mobile application for your business, well look no further, we have you covered!
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="b-infoblock-with-icon">
                    <a href="#" class="b-infoblock-with-icon__icon f-infoblock-with-icon__icon fade-in-animate">
                    <i class="fa  fa-globe"></i>
                    </a>
                    <div class="b-infoblock-with-icon__info f-infoblock-with-icon__info">
                        <a href="services.php" class="f-infoblock-with-icon__info_title b-infoblock-with-icon__info_title f-primary-sb">Logo Design</a>
                        <div class="f-infoblock-with-icon__info_text b-infoblock-with-icon__info_text">
                            We will work with you to create a simple and professional logo.
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix visible-sm-block"></div>
        </div>
    </div>
</section>
<section class="b-bg-block f-bg-block b-bg-block-meadow">
    <div class="container f-center">
        <h1 class="f-primary-b"> variety of custom software Development Services.</h1>
        <div class="b-bg-block__desc f-bg-block__desc f-primary">Web applications using PHP or Ruby, Mobile Applications, and APIs</div>
        <a class="b-btn f-btn b-btn-md f-btn-md b-btn-primary f-primary-sb j-data-element" data-animate="shake" href="portfolio.php"><i class="fa fa-desktop"></i>OUR WORK</a>
        <span class="clearfix visible-xs-block"></span>
        <a class="b-btn f-btn b-btn-md f-btn-md f-primary-sb j-data-element" data-animate="shake" href="mailto:quotes@bearcave-studios.com"><i class="fa fa-money"></i>GET A QUOTE</a>
    </div>
</section>
<section class="b-diagonal-line-bg-light b-section-info">
    <div class="container">
        <div class="row">
            <div class="b-section-info__img img-appearance-1 wrap-img-appearance col-sm-6 col-xs-12">
                <img data-retina data-animate="fadeInLeft" class="img-appearance-item-1" src="img/animation-data/imac.png" alt="imac"/>
                <img data-retina data-animate="fadeInLeft" class="img-appearance-item-2" src="img/animation-data/mac-book.png" alt="mac-book"/>
                <img data-retina data-animate="fadeInLeft" class="img-appearance-item-3" src="img/animation-data/ipad.png" alt="ipad"/>
                <img data-retina data-animate="fadeInLeft" class="img-appearance-item-4" src="img/animation-data/ipad-mini.png" alt="ipad-mini"/>
                <img data-retina data-animate="fadeInLeft" class="img-appearance-item-5" src="img/animation-data/iphone.png" alt="iphone"/>
            </div>
            <div class="b-section-info__text f-section-info__text col-sm-6 col-xs-12">
                <h2 class="f-primary-b">ARE YOU READY TO DiSCOVER?</h2>
                <p class="f-section-info__text_short f-primary-sb">Take your business to the next level of awesome</p>
                <div class="b-ol-list-text-container">
                    <div class="b-ol-list-text__item">
                        <div class="b-ol-list-text__item_number f-ol-list-text__item_number f-primary-b">1</div>
                        <div class="b-ol-list-text__item_info">
                            <a href="#" class="b-ol-list-text__item_info-title f-ol-list-text__item_info-title f-primary-sb">Partnering with you to handle and achieve</a>
                            <p class="b-ol-list-text__item_info-desc f-ol-list-text__item_info-desc">Already have a website or a design and just need someone to make it happen?  Our team will ensure your project comes to life and meets your vision.</p>
                        </div>
                    </div>
                    <div class="b-ol-list-text__item">
                        <div class="b-ol-list-text__item_number f-ol-list-text__item_number f-primary-b">2</div>
                        <div class="b-ol-list-text__item_info">
                            <a href="#" class="f-ol-list-text__item_info-title f-primary-sb">Results-driven solutions for businesses of all sizes, start-up to enterprise!</a>
                            <p class="b-ol-list-text__item_info-desc f-ol-list-text__item_info-desc">Whether you are a start-up company or well established business we will do our best to ensure your product is top quality and scalable.</p>
                        </div>
                    </div>
                    <div class="b-ol-list-text__item">
                        <div class="b-ol-list-text__item_number f-ol-list-text__item_number f-primary-b">3</div>
                        <div class="b-ol-list-text__item_info">
                            <a href="#" class="f-ol-list-text__item_info-title f-primary-sb">We specialize in creating Ruby on Rails and Php web applications.</a>
                            <p class="b-ol-list-text__item_info-desc f-ol-list-text__item_info-desc">We build your application from the ground up.  Our team focuses on fast, elegant, and extendable applications.</p>
                        </div>
                    </div>
                    <div class="clearfix">
                        <a href="#" class="f-more f-primary-b">and much more...</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="f-center b-section-info b-null-bottom-indent-xs b-gallery-main">
        <div class="container">
            <h2 class="f-primary-b">our works</h2>
            <p class="b-infoblock-description f-desc-section f-center">Here is a list of projects our team has worked on.</p>
            <div class="b-hr-stars f-hr-stars container">
                <div class="b-hr-stars__group">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
            </div>
        </div>
        <div class="b-gallery-main-container">
            <div class="col-sm-12 col-xs-12 b-gallery-main-item view view-eighth">
                <img data-retina="" src="img/portfolio1.jpg" alt="">
                <div class="b-item-hover-action f-center mask">
                    <div class="b-item-hover-action__inner">
                        <div class="b-item-hover-action__inner-btn_group">
                            <a href="img/portfolio1.jpg" class="b-btn f-btn b-btn-light f-btn-light info fancybox" title="BRB Fitness" rel="group3"><i class="fa fa-arrows-alt"></i></a>
                            <a href="http://www.brb-fitness.com" class="b-btn f-btn b-btn-light f-btn-light info"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="f-gallery-main-item__info_name f-primary-b">BRB Fitness</div>
                        <div class="f-gallery-main-item__info_desc">Revolutionary fitness company.</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="b-diagonal-line-bg-light b-section-info b-section-info__img-r f-section-info__img-r">
        <div class="container">
            <div class="row">
                <div class="b-section-info__img col-sm-6 col-xs-12">
                    <img data-retina class="j-data-element" data-animate="fadeInRight" src="img/animation-data/responsive_img_2.png" alt=""/>
                </div>
                <div class="b-section-info__text f-section-info__text col-sm-6 col-xs-12">
                    <h2 class="f-primary-b">Let’s Chat About Your Project!</h2>
                    <p class="f-section-info__text_short">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque luctus ipsum nenatis mi auctor ullamcorper.</p>
                    <hr class="b-hr-light"/>
                    <div class="b-section-info__text_group">
                        <ul class="c-primary c--inherit b-list-markers f-list-markers b-list-markers--without-leftindent f-list-markers--medium f-color-primary b-list-markers-2col f-list-markers-2col">
                            <li><a href="#"><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i> Content Management System</a></li>
                            <li><a href="#"><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i> Multi language support</a></li>
                            <li><a href="#"><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>eCommerce solutions</a></li>
                            <li><a href="#"><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i> Signups or sales</a></li>
                            <li><a href="#"><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i> custom software Development</a></li>
                            <li><a href="#"><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i> SaaS web applications</a></li>
                            <li><a href="#"><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i> and much more...</a></li>
                        </ul>
                        <a href="#" class="b-btn f-btn b-btn-default b-btn-md f-primary-b">request a quote</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- This is where the client testimonials partial should be included once needed -->
    <?php include 'partials/footer.php'; ?>