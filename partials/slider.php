<div class="l-main-container">
    <div class="b-slidercontainer">
    <div class="b-slider j-fullscreenslider">
        <ul>
            <li data-transition="3dcurtain-vertical" data-slotamount="7">
                <div class="tp-bannertimer"></div>
                <img data-retina src="img/slider/slider-lg__bg.png">
                <div class="caption sfb"  data-x="center" data-y="bottom" data-speed="700" data-start="1700" data-easing="Power4.easeOut" style="z-index: 2">
                    <img data-retina src="img/slider/slider_main_2-1.png">
                </div>
                <div class="caption sfl"  data-x="50" data-y="bottom" data-speed="700" data-start="2500" data-easing="Power4.easeOut">
                    <img data-retina src="img/slider/slider_main_2-2.png">
                </div>
                <div class="caption sfr"  data-x="right" data-y="bottom" data-hoffset="-30" data-speed="700" data-start="2500" data-easing="Power4.easeOut">
                    <img data-retina src="img/slider/slider_main_2-3.png">
                </div>
                <div class="caption lft"  data-x="center" data-y="30" data-speed="600" data-start="2600">
                    <h1 class="f-primary-b c-white" >We are an interactive agency</h1>
                </div>
                <div class="caption"  data-x="center" data-y="90" data-speed="600" data-start="3200">
                    <p class="f-primary-b f-slider-lg-item__text_desc f-center c-white" >
                       focused on results-driven solutions for start-ups to<br/>
enterprise businesses!
                    </p>
                </div>
            </li>
            <li data-transition="" data-slotamount="7">
                <div class="tp-bannertimer"></div>
                <img data-retina src="img/slider/slider-lg__bg.png">
                <div class="caption sfb"  data-x="right" data-y="bottom" data-speed="700" data-start="1700" data-easing="Power4.easeOut">
                    <img data-retina src="img/slider/slider-ipad.png">
                </div>
                <div class="caption sfl"  data-x="right" data-y="bottom" data-speed="700" data-start="2500" data-easing="Power4.easeOut" style="z-index: 2">
                    <img data-retina src="img/slider/slider-iphone.png">
                </div>
                <div class="caption lft"  data-x="left" data-y="112" data-speed="600" data-start="2600">
                    <h1 class="f-primary-b c-white" >Get more internet traffic<br> to your website
</h1>
                    <h3 class="f-primary-b c-white">Results-driven solutions for<br> start-ups to enterprise businesses!</h3>
                </div>
                
                <div class="caption"  data-x="left" data-y="305" data-speed="800" data-start="3600">
                    <a href="contact.php"><button class="button-lg">CONTACT TODAY</button></a>
                </div>
            </li>
            <li data-transition="" data-slotamount="7">
                <div class="tp-bannertimer"></div>
                <img data-retina src="img/slider/slider-lg__bg.png">
                <div class="caption sfb"  data-x="right" data-y="70" data-speed="700" data-start="1700" data-easing="Power4.easeOut" style="z-index: 2">
                    <img data-retina src="img/slider/slider-imac.png">
                </div>
                <div class="caption lft"  data-x="left" data-y="95" data-speed="600" data-start="2600">
                    <h2 class="f-primary c-white">Bearcave Studio </h2>
                </div>
                <div class="caption lft"  data-x="left" data-y="135" data-speed="600" data-start="2600">
                    <h1 class="f-primary-b c-white b-bg-slider-title">modern. creative. unique</h1>
                </div>
                <div class="caption lft"  data-x="left" data-y="220" data-speed="600" data-start="2600">
                    <ul class="b-slider-list f-slider-list c-white">
                        <li>
                            <i class="fa fa-tablet"></i> <span>focused on results-driven solutions for start-ups to
enterprise businesses!</span>
                        </li>
                        <li>
                            <i class="fa fa-cog"></i> <span>Websites, User Dashboard, Admin Portals, </span>
                        </li>
                        <li>
                            <i class="fa fa-trophy"></i> <span>intuitive designs and workflow for the User Experience!</span>
                        </li>
                        <li>
                            <i class="fa fa-globe"></i> <span>Awesome icons included</span>
                        </li>
                       
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>
    