<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo $page_title ?></title>
    
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<!-- bxslider -->
<link type="text/css" rel='stylesheet' href="js/bxslider/jquery.bxslider.css">
<!-- End bxslider -->
<!-- flexslider -->
<link type="text/css" rel='stylesheet' href="js/flexslider/flexslider.css">
<!-- End flexslider -->

<!-- bxslider -->
<link type="text/css" rel='stylesheet' href="js/radial-progress/style.css">
<!-- End bxslider -->

<!-- Animate css -->
<link type="text/css" rel='stylesheet' href="css/animate.css">
<!-- End Animate css -->

<!-- Bootstrap css -->
<link type="text/css" rel='stylesheet' href="css/bootstrap.min.css">
<link type="text/css" rel='stylesheet' href="js/bootstrap-progressbar/bootstrap-progressbar-3.2.0.min.css">
<!-- End Bootstrap css -->

<!-- Jquery UI css -->
<link type="text/css" rel='stylesheet' href="js/jqueryui/jquery-ui.css">
<link type="text/css" rel='stylesheet' href="js/jqueryui/jquery-ui.structure.css">
<!-- End Jquery UI css -->

<!-- Fancybox -->
<link type="text/css" rel='stylesheet' href="js/fancybox/jquery.fancybox.css">
<!-- End Fancybox -->

<link type="text/css" rel='stylesheet' href="fonts/fonts.css">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

<link type="text/css" data-themecolor="default" rel='stylesheet' href="css/main-default.css">

<link type="text/css" rel='stylesheet' href="js/rs-plugin/css/settings.css">
<link type="text/css" rel='stylesheet' href="js/rs-plugin/css/settings-custom.css">

</head>
<body>
<!-- <div class="mask-l" style="background-color: #fff; width: 100%; height: 100%; position: fixed; top: 0; left:0; z-index: 9999999;"></div> --><!--removed by integration-->
<header>
  <div class="container b-header__box b-relative">
    <a href="/" class="b-left b-logo "><img class="color-theme" data-retina src="img/logo-header-default.png" alt="Logo" /></a>
    <div class="top_right">
    <div class="top_contact"><i class="fa fa-phone"></i> 605-280-4105</div><div class="top_contact"><i class="fa fa-envelope"></i> contact@bearcavestudio.com</div>
      <div class="top_social"><a href="http://www.facebook.com/BearCaStudios"><i class="fa  fa-facebook"></i></a><a href="#"><i class="fa  fa-twitter"></i></a><a href="#"><i class="fa  fa-youtube-square"></i></a></div>
    </div>
    <div class="b-header-r b-right b-header-r--icon">
        
    
      
      <div class="b-top-nav-show-slide f-top-nav-show-slide b-right j-top-nav-show-slide"><i class="fa fa-align-justify"></i></div>
      <nav class="b-top-nav f-top-nav b-right j-top-nav">
          <ul class="b-top-nav__1level_wrap">
    <li class="b-top-nav__1level f-top-nav__1level f-primary-b <?php if($section == "index") {echo "is-active-top-nav__1level";} ?>"><a href="index.php><i class="fa fa-home b-menu-1level-ico"></i>Home </a>
        
    </li>
    <li class="b-top-nav__1level f-top-nav__1level f-primary-b <?php if($section == "about") {echo "is-active-top-nav__1level";} ?>">
        <a href="about.php"><i class="fa fa-folder-open b-menu-1level-ico"></i>About Us</a>
       
    </li>
    <li class="b-top-nav__1level f-top-nav__1level f-primary-b <?php if($section == "services") {echo "is-active-top-nav__1level";} ?>">
        <a href="services.php"><i class="fa fa-picture-o b-menu-1level-ico"></i> Services </a>
        
    </li>
        
    <li class="b-top-nav__1level f-top-nav__1level f-primary-b <?php if($section == "portfolio") {echo "is-active-top-nav__1level";} ?>">
        <a href="portfolio.php"><i class="fa fa-inbox   b-menu-1level-ico"></i>Portfolio </a>
        
    </li>
     <li class="b-top-nav__1level f-top-nav__1level f-primary-b <?php if($section == "contact") {echo "is-active-top-nav__1level";} ?>">
        <a href="contact.php"><i class="fa fa-inbox   b-menu-1level-ico"></i>Contact </a>
        
    </li>
    
    
        </ul>
      </nav>
    </div>
  </div>
</header>